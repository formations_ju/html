---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "html.png"
slideNumber: true
title: "VSCode Reveal intro"
---

## Formation HTML

---

## Etapes

- présentation <small>classique mais obligatoire</small>
- structure <small>encore de la théorie</small>
- quelques balises <small>encore et toujours des balises</small>
- exercices <small>pour faire chauffer le cerveau</small>
- projet <small>pour voir plus loin</small>

---

## Questions

**Qu'est-ce que** le html ?

<small>langage ou pas langage?!</small>

**Ou** trouve-t-on le html ?

<small>serveur ou navigateur ?!</small>

**Comment** fonctionne le html ?

<small>balise ouverte ou fermée ?!</small>

**Comment** on fait pour connaitre toutes les balises?

<small>RTFM</small>

---

## Présentation

HTML -> HyperText Markup Language

le HTML est un langage de description.

On va le trouver dans le navigateur.

C'est un langage à balise similaire au xml.

---

## structure

structure de base
```html
<balise ouvrante> contenu </balise fermante>
```
ce groupe s'appelle un **élément**

enchainement possible

```html
<balise 1> 
    contenu 
    <balise 2> contenu 2</balise 2> 
    fin contenu 1 
</balise 1>
```

<span style="color: #FF0000"> erreur </span>

```html
<balise 1> 
    contenu 
    <balise 2> contenu 2</balise 1> 
    fin contenu 1 
</balise 2>
```

--

## Structure (suite)

la balise autofermée ou balise sans contenu

```html
<balise autofermé>
```

--

## Structure (suite)

```html
<balise attribut="valeur"> contenu </balise>
```

il existe des attributs spécifiques à certaines balises et des attributs universels à toutes les balises.

---

## Quelques balises

* ```<p>``` -> paragraphe
* ```<img>``` -> image
* ```<hX>``` -> header
* ```<a>``` -> anchor

### à retrouver dans la doc

En route pour la [doc](https://developer.mozilla.org/fr/docs/Web/HTML)

MDN -> documentation technique

W3C -> norme

---

## Exercice

* parcourir la documentation en fonction de ses besoins.
* faire une note de cours en HTML(utiliser un maximum de balise)
* faire valider son html avec le W3C

---

## Complément

balise ```head```

encodage UTF-8

Caractère réservé

formulaire

---

## Mini-projet

Créer un site fictif en pur HTML.
* blog
* gestionnaire de livre, dvd, ...
* site de présentation(restaurant, vente de chaussure, ...)

Obligation d'utiliser un maximum de balise.

```<p> <div> <img> <a> <table> <ul> <iframe> <hX> <header> <footer> ```

toujours plus [d'images](https://unsplash.com/)